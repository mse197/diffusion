#Calculates the commit-based points for each student:
import subprocess

#Call a git command, parse the output, and put the names/commits into a dictionary
cmd = 'git log --oneline --cherry-pick --pretty="%an" --branches=* | sort |  uniq -c | sort -r -g'
a = subprocess.check_output(cmd,shell=True)
lines = a.split('\n')
commits = {}
total = 0
for l in lines:
    p = l.split()
    if len(p) > 0:
        n = int(p[0])
        total+=n
        name = ""
        for i in p[1:]:
            name = name+i+" "
        name = name[:-1]
        commits[name]=n

#Handle leticia's two usernames:
commits['leticialeite'] += commits['Leticia Leite de Paiva']
del commits['Leticia Leite de Paiva']

#Print the output
print "There were {} total commits\n".format(total)
print "Points scored for commits made to Project 1:\n"
print "NAME                  NUMBER   POINTS"
for k,n in commits.iteritems():
    if n>10:
        nc = 10
    else:
        nc = n
    print "{:16}\t{}\t{}/30".format(k,n,nc*3)


    
