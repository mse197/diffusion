# Implementing eric's model for diffusion
# We have molecules that switch back and forth between two tanks
# We need to keep track of each molecule's state
# If a molecule is in tank 0 we'll label it with a 0, and 1 if in 1

# Imports the pseudo-random integer number generator from random module
from random import randint

# Imports scientific computing package module
import numpy

# Imports plotting functions from matplotlib module
from matplotlib import pyplot

# Allows for plot label function
 # Renames the import to 'plt'
  # Helps to reduce namespace conflicts
import matplotlib.pyplot as plt

# Assigns initial parameters for our diffusion model

#input the number of molecules in the system
n  = int (input ('Enter the number of molecules of the system: '))
#input the number of molecules in tank 1
x = int (input ('Enter the number of molecules that are in tank 1: '))
#calculates the number of molecules on tank 0
y  = n - x
#print the number of molecules on tank 0
print ('The number of molecules that are in tank 0 is:'), y

#promt for the number of time steps to simulate
n_steps = int (input('Enter the number of time steps to carry out:'))

#this chunk of code creates and initializes an array to hold the molecules and which tank they are in (0 = tank 0, 1 = tank 1) 
molecules = []
for i in range(x):
    molecules.append(1)
for i in range(y):
    molecules.append(0)
molecules = numpy.array(molecules)

average = numpy.zeros(n_steps)
ave_of_ave = []

# Creates and opens a file titled "step_data"
 # 'w' arguement allows for writing as well as truncating if it exists
  # Assigns a variable "step_output" to the "step_data" file
   #  
step_output = open('step_data', 'w')

#This loop randomly picks a number m  from  0 to the length of the array that corresponds to the number of total molecules selected.
# This random number becomes the index for which molecule will be moved.  molecules[m] calls the value of the molecule at index m. The value is then reassigned.
for step in range(n_steps):
	m = randint(0,n-1)
	if molecules[m] == 1:
		molecules[m] = 0
	else:
		molecules[m] = 1
	average[step] = molecules.mean()
	if step > 0:
		ave_of_ave.append(average[0:step].mean())
	print >> step_output, step, 'Molecules in Tank 1', molecules.sum(), 'average', average[step]
	#Removes array and prints number of molecules in Tank 1

# Closes the file "step_output" after all calculations are finished
step_output.close()

# Prints each state of 'molecules' to a txt file titled step_data

# average[step] averages the 1's and 0's in the array for each individual step.
# ave_of_ave.append(average[0:step].mean()) averages the average of the steps.
# so that we receive one average instead of an average for each step.
# This way we can quickly determine if our model is working. If it is working, our average should be 0.5.

# Output the mean of the averages to the screen
print average.mean()

#Ask user if they want a plot generated
plotoutput = raw_input(("Do you want to plot the output? y/n: "))

# Outputs a plot

#This checks if the input string matches any elements in the array, ['y', 'Y', 'yes', 'Yes'] to give a little more response  flexibility. 
#If the user enters yes, then the lines of code that create a plot are executed, if the user enters anything else, the program will exit. 
if plotoutput in [ 'y','Y','yes','Yes']:

# Produces the plot of the for loop calculation
	pyplot.plot(ave_of_ave)

# Adds x-axis label
	pyplot.xlabel('number of time steps')

# Adds y-axis label
	pyplot.ylabel ('average of the averages')

# Outputs a plot
	pyplot.show()


else:
	exit

	

